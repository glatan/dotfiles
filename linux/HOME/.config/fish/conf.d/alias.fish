function define_alias
    if command -s $argv[1] > /dev/null
        alias $argv[2] $argv[3]
    else
        echo "Unknown command: $1"
    end
end

define_alias 'cargo' c 'cargo'
define_alias 'deno' d 'deno'
define_alias 'docker' dc 'docker compose'
define_alias 'git' g 'git'
define_alias 'git' gc 'git clone'
define_alias 'git' gcom 'git commit'
define_alias 'git' gs 'git switch'
define_alias 'git' gst 'git status'
define_alias 'make' m 'make'
define_alias 'npm' n 'npm'
define_alias 'npm' nr 'npm run'
define_alias 'podman' p 'podman'
define_alias 'rustup' rup 'rustup'
