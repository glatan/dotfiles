#!/usr/bin/env bash

declare CACHE_DIRECTORY="${HOME}/.cache/dotfiles"

declare APPLY_HOME=0
declare APPLY_ROOT=0
declare INSTALL_PACKAGES=0

[[ ! $(command -v git) ]] && echo 'Command not found: git' && exit 1
[[ ! $(command -v pacman) ]] && echo 'Command not found: pacman' && exit 1
[[ ! $(command -v rsync) ]] && echo 'Command not found: rsync' && exit 1

_get_source() {
    [[ ! -d "${CACHE_DIRECTORY}" ]] \
        && mkdir -p "${CACHE_DIRECTORY}" \
        && git clone https://gitlab.com/glatan/dotfiles.git "${CACHE_DIRECTORY}"
    cd "${CACHE_DIRECTORY}" || exit 1
    git pull
}

_apply_home() {
    rsync -r --chmod a=,u+rXw "${CACHE_DIRECTORY}/linux/HOME/" "${HOME}"
}

_apply_root() {
    [[ ! ${UID} == 0 ]] && echo 'Please run as root' && exit 1
    rsync -r --chmod a=rX,u+w --chown=root:root ./linux/etc /
}

_install_packages() {
    [[ ! ${UID} == 0 ]] && echo 'Please run as root' && exit 1
    pacman -Syyu
    source "${CACHE_DIRECTORY}/linux/depends.conf"
    for package in "${depends[@]}"; do
        pacman -Qn "${package}" > /dev/null || pacman -S "${package}"
    done
}

_usage() {
    printf "Options:\n"
    printf "\t--all\t\t\tEnable all options\n"
    printf "\t--apply_home\t\tInstall files to home directory\n"
    printf "\t--apply_root\t\tInstall files to root directory\n"
    printf "\t--install-packages\tInstall configured packages\n"
}

_main() {
    if [[ "${#@}" -eq "" ]]; then
        _usage
        exit 0
    fi
    for arg in "$@"; do
        case "${arg}" in
            --all)
                APPLY_HOME=1
                APPLY_ROOT=1
                INSTALL_PACKAGES=1
            ;;
            --apply-home)
                APPLY_HOME=1
            ;;
            --apply-root)
                APPLY_ROOT=1
            ;;
            --debug)
                if git remote &> /dev/null; then
                    remote_url="$(git remote get-url origin)"
                    if [[ ${remote_url} == 'git@gitlab.com:glatan/dotfiles.git' || ${remote_url} == 'https://gitlab.com/glatan/dotfiles.git' ]]; then
                        CACHE_DIRECTORY="$(git rev-parse --show-toplevel)"
                    else
                        printf "This option can only enable under this project's directory.\n"
                        exit 1
                    fi
                else
                    printf "This option can only enable under this project's directory.\n"
                    exit 1
                fi
                set -x
            ;;
            --install-packages)
                INSTALL_PACKAGES=1
            ;;
            --help)
                _usage
                exit 0
            ;;
            *)
                _usage
                exit 1
            ;;
        esac
    done
    if [[  ${APPLY_HOME} -eq 1 || ${APPLY_ROOT} -eq 1 || ${INSTALL_PACKAGES} -eq 1 ]]; then
        _get_source
        if [[ ${APPLY_HOME} -eq 1 ]]; then
            _apply_home
        fi
        if [[ ${APPLY_ROOT} -eq 1 ]]; then
            _apply_root
        fi
        if [[ ${INSTALL_PACKAGES} -eq 1 ]]; then
            _install_packages
        fi
    fi
    exit 0
}

_main "$@"
